package de.pdg.igf.util;

import java.util.Collection;

public class VectorHandler {

    public static String toString(java.util.Vector vv, Boolean filesOnly){
        String retString = "";
        if (vv != null) {
            for (int ii = 0; ii < vv.size(); ii++) {
                Object obj = vv.elementAt(ii);
                if (obj instanceof com.jcraft.jsch.ChannelSftp.LsEntry) {
                    if(!((com.jcraft.jsch.ChannelSftp.LsEntry)obj).getFilename().equals(".")
                        && !((com.jcraft.jsch.ChannelSftp.LsEntry)obj).getFilename().equals("..")){
                        
                        if(filesOnly){
                            if(!((com.jcraft.jsch.ChannelSftp.LsEntry)obj).getAttrs().isDir()
                                && !((com.jcraft.jsch.ChannelSftp.LsEntry)obj).getAttrs().isLink()){
                                retString =
                                    retString + ((com.jcraft.jsch.ChannelSftp.LsEntry)obj).getFilename() + ";";
                            }
                        }else{
                            retString =
                                retString + ((com.jcraft.jsch.ChannelSftp.LsEntry)obj).getFilename() + ";";
                        }
                    }
                }
            }
        }
        
        if (retString.length() < 1){
            return retString;
        } else{
            return retString.substring(0, retString.length()-1);
        }
        
    }
    
    public static String[] toStringArray(java.util.Vector vv, Boolean filesOnly){
        String[] ret_array = new String[vv.size()];
        
        if (vv != null) {
            for (int ii = 0; ii < vv.size(); ii++) {
                Object obj = vv.elementAt(ii);
                if (obj instanceof com.jcraft.jsch.ChannelSftp.LsEntry) {
                    if(!((com.jcraft.jsch.ChannelSftp.LsEntry)obj).getFilename().equals(".")
                        && !((com.jcraft.jsch.ChannelSftp.LsEntry)obj).getFilename().equals("..")){
                        if(filesOnly){
                            if(!((com.jcraft.jsch.ChannelSftp.LsEntry)obj).getAttrs().isDir()
                                && !((com.jcraft.jsch.ChannelSftp.LsEntry)obj).getAttrs().isLink()){
                                ret_array[ii] = ((com.jcraft.jsch.ChannelSftp.LsEntry)obj).getFilename();
                            }
                        }else{
                            ret_array[ii] = ((com.jcraft.jsch.ChannelSftp.LsEntry)obj).getFilename();
                        }
                    }
                }
            }
        }
        
        return ret_array;
    }
}
