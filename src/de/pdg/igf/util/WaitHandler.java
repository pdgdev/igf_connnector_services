package de.pdg.igf.util;

import de.pdg.igf.log.Logger;

public class WaitHandler {
    public static String waitForSeconds(long seconds) throws InterruptedException {
        
        Logger.info("WaitHandler.waitForSeconds..." + "\r\n" +
                            "  -> seconds: " + seconds);
        
        String successMessage = "Successfully waited for " + seconds + " seconds.";
        
        long millis = seconds * 1000;
        Thread.sleep(millis);
        Logger.info("WaitHandler.waitForSeconds succeeded: " + successMessage);  
        return successMessage;
    }
}
