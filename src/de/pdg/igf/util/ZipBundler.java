package de.pdg.igf.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


public class ZipBundler {
    private ByteArrayOutputStream baos;
    private ZipOutputStream zos;
    
    public ZipBundler(){
        baos = new ByteArrayOutputStream();
        zos = new ZipOutputStream(baos);
    }
    
    public void addEntry(String filename, byte[] input) throws IOException {
        ZipEntry entry = new ZipEntry(filename);
        entry.setSize(input.length);
        zos.putNextEntry(entry);
        zos.write(input);
        zos.closeEntry();
    }
    
    public byte[] getBundle() throws IOException {
        zos.close();
        return baos.toByteArray();
    }
    
    public void clearBundle() throws IOException {
        baos = new ByteArrayOutputStream();
        zos = new ZipOutputStream(baos);
    }
    
}
