package de.pdg.igf.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex {
    public static Boolean regexCheck(String stringToCheck, String regexPattern){
        Boolean isMatching = false;
        Pattern p = Pattern.compile(regexPattern);
        Matcher m = p.matcher(stringToCheck);
        
        isMatching = m.find();
        
        return isMatching;
    }
}
