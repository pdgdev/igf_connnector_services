package de.pdg.igf.util.dao;

public class UtilResponse {
    private static String jsonBasic = "{\"UtilResponse\":{\"Response\":\"$1\",\"Error-Message\":\"$2\"}}";
    
    public static String getUtilResponse(String response, String message){
        String jsonResponse = "";
        jsonResponse = jsonBasic.replace("$1", response);
        jsonResponse = jsonResponse.replace("$2", message);
        return jsonResponse;
    }
}

