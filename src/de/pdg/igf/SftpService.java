package de.pdg.igf;


import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;

import de.pdg.igf.jdbc.JdbcCommunicator;
import de.pdg.igf.log.Logger;
import de.pdg.igf.sftp.Sftp;
import de.pdg.igf.sftp.dao.SftpResponse;

import java.io.IOException;

import java.nio.file.AccessDeniedException;

import java.sql.SQLException;

import java.util.ArrayList;

import javax.naming.NamingException;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.ext.Provider;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("sftp")
public class SftpService {
    public static Boolean testMode = false;
    
    @GET
    @Produces("plain/text")
    @Path("/ls")
    public String ls(@HeaderParam("p_ftp_host") String host,
                     @HeaderParam("p_ftp_username") String username,
                     @HeaderParam("p_ftp_password") String password,
                     @HeaderParam("p_ftp_keypath") String keypath,
                     @HeaderParam("p_ftp_keyphrase") String keyphrase,
                     @HeaderParam("p_path") String path,
                     @HeaderParam("p_filesOnly") String filesOnly) {
        Sftp sftp = new Sftp(username, password, host, keypath, keyphrase);
        String response = "";
        try {
            Logger.populateGlobalOperationIdentifier("LS");
            Logger.info("Executing LS with" + 
                         " p_ftp_host:" + host + 
                         " p_ftp_username:" + username + 
                         " p_ftp_keypath:" + keypath + 
                         " p_path:" + path +
                         " p_filesOnly:" + filesOnly);
            
            sftp.connect();
            
            response = provideAndStoreResponse(SftpResponse.getSftpResponse(sftp.ls(path,filesOnly.equals("Y")?true:false), ""));
            return response;
        } catch (JSchException e) {
            return provideAndStoreFormattedExcpetion(e);
        } catch (SftpException e) {
            return provideAndStoreFormattedExcpetion(e);
        } catch (Exception e) {
            return provideAndStoreFormattedExcpetion(e);
        } finally {
            sftp.disconnect();    
            }
    }

    @GET
    @Produces("plain/text")
    @Path("/lsla")
    public String lsla(@HeaderParam("p_ftp_host") String host,
                       @HeaderParam("p_ftp_username") String username,
                       @HeaderParam("p_ftp_password") String password,
                       @HeaderParam("p_ftp_keypath") String keypath,
                       @HeaderParam("p_ftp_keyphrase") String keyphrase,
                       @HeaderParam("p_path") String path,
                       @HeaderParam("p_filesOnly") String filesOnly) {
        Sftp sftp = new Sftp(username, password, host, keypath, keyphrase);
        String response = "";
        try {
            Logger.populateGlobalOperationIdentifier("LSLA");
            Logger.info("Executing LSLA with" + 
                         " p_ftp_host:" + host + 
                         " p_ftp_username:" + username + 
                         " p_ftp_keypath:" + keypath + 
                         " p_path:" + path);
            
            sftp.connect();
            
            response = provideAndStoreResponse(SftpResponse.getSftpResponse(sftp.lsla(path,filesOnly.equals("Y")?true:false), ""));
            return response;
        } catch (JSchException e) {
            return provideAndStoreFormattedExcpetion(e);
        } catch (SftpException e) {
            return provideAndStoreFormattedExcpetion(e);
        } finally{
            sftp.disconnect();    
            }
    }

    @GET
    @Produces("plain/text")
    @Path("/mkdir")
    public String mkdir(@HeaderParam("p_ftp_host") String host,
                        @HeaderParam("p_ftp_username") String username,
                        @HeaderParam("p_ftp_password") String password,
                        @HeaderParam("p_ftp_keypath") String keypath,
                        @HeaderParam("p_ftp_keyphrase") String keyphrase,
                        @HeaderParam("p_path") String path) {
        Sftp sftp = new Sftp(username, password, host, keypath, keyphrase);
        String response = "";
        try {
            Logger.populateGlobalOperationIdentifier("MKDIR");
            Logger.info("Executing MKDIR with" + 
                         " p_ftp_host:" + host + 
                         " p_ftp_username:" + username + 
                         " p_ftp_keypath:" + keypath + 
                         " p_path:" + path);
            
            sftp.connect();
            response = SftpResponse.getSftpResponse(sftp.mkdir(path), "");
            return response;
        } catch (JSchException e) {
            return provideAndStoreFormattedExcpetion(e);
        } catch (SftpException e) {
            return provideAndStoreFormattedExcpetion(e);
        } catch (Exception e) {
            return provideAndStoreFormattedExcpetion(e);
        } finally{
            sftp.disconnect();        
            }
    }

    @GET
    @Produces("plain/text")
    @Path("/rmdir")
    public String rmdir(@HeaderParam("p_ftp_host") String host,
                        @HeaderParam("p_ftp_username") String username,
                        @HeaderParam("p_ftp_password") String password,
                        @HeaderParam("p_ftp_keypath") String keypath,
                        @HeaderParam("p_ftp_keyphrase") String keyphrase,
                        @HeaderParam("p_path") String path) {
        String response = "";
        Sftp sftp = new Sftp(username, password, host, keypath, keyphrase);
        try {
            Logger.populateGlobalOperationIdentifier("RMDIR");
            Logger.info("Executing RMDIR with" + 
                         " p_ftp_host:" + host + 
                         " p_ftp_username:" + username + 
                         " p_ftp_keypath:" + keypath + 
                         " p_path:" + path);
            
            sftp.connect();
            response = provideAndStoreResponse(SftpResponse.getSftpResponse(sftp.rmdir(path), ""));
            return response;
        } catch (JSchException e) {
            return provideAndStoreFormattedExcpetion(e);
        } catch (SftpException e) {
            return provideAndStoreFormattedExcpetion(e);
        } catch (Exception e) {
            return provideAndStoreFormattedExcpetion(e);
        } finally {
            sftp.disconnect();    
            }
    }

    @GET
    @Produces("plain/text")
    @Path("/rm")
    public String rm(@HeaderParam("p_ftp_host") String host,
                     @HeaderParam("p_ftp_username") String username,
                     @HeaderParam("p_ftp_password") String password,
                     @HeaderParam("p_ftp_keypath") String keypath,
                     @HeaderParam("p_ftp_keyphrase") String keyphrase,
                     @HeaderParam("p_path") String path) {
        Sftp sftp = new Sftp(username, password, host, keypath, keyphrase);
        String response = "";
        try {
            Logger.populateGlobalOperationIdentifier("RM");
            Logger.info("Executing RM with" + 
                         " p_ftp_host:" + host + 
                         " p_ftp_username:" + username + 
                         " p_ftp_keypath:" + keypath + 
                         " p_path:" + path);
            
            sftp.connect();
            response = provideAndStoreResponse(SftpResponse.getSftpResponse(sftp.rm(path), ""));
            return response;
        } catch (JSchException e) {
            return provideAndStoreFormattedExcpetion(e);
        } catch (SftpException e) {
            return provideAndStoreFormattedExcpetion(e);
        } catch (Exception e) {
            return provideAndStoreFormattedExcpetion(e);
        } finally{
            sftp.disconnect();  
            }
    }

    @GET
    @Produces("plain/text")
    @Path("/mv")
    public String mv(@HeaderParam("p_ftp_host") String host,
                     @HeaderParam("p_ftp_username") String username,
                     @HeaderParam("p_ftp_password") String password,
                     @HeaderParam("p_ftp_keypath") String keypath,
                     @HeaderParam("p_ftp_keyphrase") String keyphrase,
                     @HeaderParam("p_old_path") String old_path,
                     @HeaderParam("p_new_path") String new_path) {
        Sftp sftp = new Sftp(username, password, host, keypath, keyphrase);
        String response = "";
        try {
            Logger.populateGlobalOperationIdentifier("MV");
            Logger.info("Executing MV with" + 
                         " p_ftp_host:" + host + 
                         " p_ftp_username:" + username + 
                         " p_ftp_keypath:" + keypath + 
                         " p_old_path:" + old_path +
                         " p_new_path:" + new_path);
            
            sftp.connect();
            response = provideAndStoreResponse(SftpResponse.getSftpResponse(sftp.mv(old_path, new_path),""));
            return response;
        } catch (JSchException e) {
            return provideAndStoreFormattedExcpetion(e);
        } catch (SftpException e) {
            return provideAndStoreFormattedExcpetion(e);
        } catch (Exception e) {
            return provideAndStoreFormattedExcpetion(e);
        } finally {
            sftp.disconnect();    
            }
    }

    @GET
    @Produces("plain/text")
    @Path("/get_b64")
    public String getBase64(@HeaderParam("p_ftp_host") String host,
                            @HeaderParam("p_ftp_username") String username,
                            @HeaderParam("p_ftp_password") String password,
                            @HeaderParam("p_ftp_keypath") String keypath,
                            @HeaderParam("p_ftp_keyphrase") String keyphrase,
                            @HeaderParam("p_path") String path) {
        Sftp sftp = new Sftp(username, password, host, keypath, keyphrase);
        String response = "";
        try {
            Logger.populateGlobalOperationIdentifier("GET_B64");
            Logger.info("Executing GET_B64 with" + 
                         " p_ftp_host:" + host + 
                         " p_ftp_username:" + username + 
                         " p_ftp_keypath:" + keypath + 
                         " p_path:" + path);
            
            sftp.connect();
            response = provideAndStoreResponse(SftpResponse.getSftpResponse(sftp.get_base64(path), ""));
            return response;
        } catch (JSchException e) {
            return provideAndStoreFormattedExcpetion(e);
        } catch (SftpException e) {
            return provideAndStoreFormattedExcpetion(e);
        } catch (IllegalArgumentException e) {
            return provideAndStoreFormattedExcpetion(e);
        } catch (Exception e) {
            return provideAndStoreFormattedExcpetion(e);
        } finally{
            sftp.disconnect();
            }
    }

    @GET
    @Produces("plain/text")
    @Path("/get_jndi")
    public String getJndi(  @HeaderParam("p_datasource_alias") String dataSourceAlias,
                            @HeaderParam("p_ftp_host") String host,
                            @HeaderParam("p_ftp_username") String username,
                            @HeaderParam("p_ftp_password") String password,
                            @HeaderParam("p_ftp_keypath") String keypath,
                            @HeaderParam("p_ftp_keyphrase") String keyphrase,
                            @HeaderParam("p_path") String path,
                            @HeaderParam("p_file_pattern") String fileNamePattern) {
        Sftp sftp = new Sftp(username, password, host, keypath, keyphrase);
        String response = "";
        String[] fileListing;
        JdbcCommunicator jdbc = new JdbcCommunicator();
        String returnString = "";
        
        try {
            Logger.populateGlobalOperationIdentifier("GET_JNDI");
            Logger.info("Executing GET_JNDI with" + 
                         " p_datasource_alias:" + dataSourceAlias + 
                         " p_ftp_host:" + host + 
                         " p_ftp_username:" + username + 
                         " p_ftp_keypath:" + keypath + 
                         " p_path:" + path +
                         " p_file_pattern:" + fileNamePattern);
            
            //Eestablishing the JDBC connection inside the communicator
            jdbc.establishJdbcConnection(dataSourceAlias, testMode);
            
            //Get a file listing for ftp_path
            sftp.connect();
            fileListing = sftp.lsArray(path, true);
            
            //download(sftp) and upload(jdbc) based on regex-check
            if(fileListing != null && fileListing.length > 0){
                returnString = sftp.storeSingleFilesNoncompressed(fileListing, fileNamePattern, path, jdbc, sftp);

            return provideAndStoreResponse(returnString);
            }else{
                response = provideAndStoreResponse(
                    SftpResponse.getSftpResponse("No files found in the directory: " + path + "." , "")
                    );

                return response;
            }
        } catch (JSchException e) {
            return provideAndStoreFormattedExcpetion(e);
        } catch (SftpException e) {
            return provideAndStoreFormattedExcpetion(e);
        } catch (IllegalArgumentException e) {
            return provideAndStoreFormattedExcpetion(e);
        } catch (SQLException e){
            return provideAndStoreFormattedExcpetion(e);
        } catch (IOException e){
            return provideAndStoreFormattedExcpetion(e);  
        } catch (ClassNotFoundException e){
            return provideAndStoreFormattedExcpetion(e);  
        } catch (NamingException e){
            return provideAndStoreFormattedExcpetion(e);      
        } catch (Exception e) {
            return provideAndStoreFormattedExcpetion(e);
        } finally {
            sftp.disconnect();
            
            //SEASUPPIP-72: Reliably closing the jdbc connection in a finally block.
            jdbc.closeJdbcConnection();
            }
    }

    @POST
    @Produces("plain/text")
    @Path("/put_b64")
    public String putBase64(@HeaderParam("p_ftp_host") String host,
                            @HeaderParam("p_ftp_username") String username,
                            @HeaderParam("p_ftp_password") String password,
                            @HeaderParam("p_ftp_keypath") String keypath,
                            @HeaderParam("p_ftp_keyphrase") String keyphrase,
                            @HeaderParam("p_path") String path,
                            String base64File) {
        Sftp sftp = new Sftp(username, password, host, keypath, keyphrase);
        String response = "";
        try {
            Logger.populateGlobalOperationIdentifier("PUT_B64");
            Logger.info("Executing PUT_B64 with" + 
                         " p_ftp_host:" + host + 
                         " p_ftp_username:" + username + 
                         " p_ftp_keypath:" + keypath + 
                         " p_path:" + path +
                         " p_b64_file.length:" + base64File.length());
            
            base64File = base64File.replace("\r\n", "");
            base64File = base64File.replace("\n", "");
            base64File = base64File.replace("\r", "");
            sftp.connect();
            response = provideAndStoreResponse(SftpResponse.getSftpResponse(sftp.put(path, base64File), ""));
            return response;
        } catch (JSchException e) {
            return provideAndStoreFormattedExcpetion(e);
        } catch (SftpException e) {
            return provideAndStoreFormattedExcpetion(e);
        } catch (IllegalArgumentException e) {
            return provideAndStoreFormattedExcpetion(e);
        } catch (Exception e) {
            return provideAndStoreFormattedExcpetion(e);
        } finally{
            sftp.disconnect();  
            }
    }
    
    @GET
    @Produces("plain/text")
    @Path("/put_jndi")
    public String putJndi(  @HeaderParam("p_ftp_host") String host,
                            @HeaderParam("p_ftp_username") String username,
                            @HeaderParam("p_ftp_password") String password,
                            @HeaderParam("p_ftp_keypath") String keypath,
                            @HeaderParam("p_ftp_keyphrase") String keyphrase,
                            @HeaderParam("p_datasource_alias") String dataSourceAlias,
                            @HeaderParam("p_path") String path,
                            @HeaderParam("p_token") String token) {
            Sftp sftp = new Sftp(username, password, host, keypath, keyphrase);
            String response = "";
            JdbcCommunicator jdbc = new JdbcCommunicator();
            int counter = 0;
            ArrayList<byte[]> byteArrayList;
            
            try {
                Logger.populateGlobalOperationIdentifier("PUT_JNDI");
                Logger.info("Executing PUT_JNDI with" + 
                             " p_ftp_host:" + host + 
                             " p_ftp_username:" + username + 
                             " p_ftp_keypath:" + keypath +
                             " p_datasource_alias:" + dataSourceAlias + 
                             " p_path:" + path +
                             " p_token:" + token);
                
                //Eestablishing the JDBC connection inside the communicator
                jdbc.establishJdbcConnection(dataSourceAlias, testMode);
                
                sftp.connect();
                
                byteArrayList = jdbc.getFilesFromBlob(token);
                
                for (byte[] b: byteArrayList){
                    sftp.put(path, b);
                    counter++;
                }
                
                response = provideAndStoreResponse(
                            SftpResponse.getSftpResponse("Token=" + token + " -> " + counter + " files successfully uploaded to " + path, "")
                            );
                return response;
                
            } catch (SQLException e) {
                return provideAndStoreFormattedExcpetion(e);
            } catch (JSchException e) {
                return provideAndStoreFormattedExcpetion(e);
            } catch (SftpException e) {
                return provideAndStoreFormattedExcpetion(e);
            } catch (IllegalArgumentException e) {
                return provideAndStoreFormattedExcpetion(e);
            } catch (NamingException e){
                return provideAndStoreFormattedExcpetion(e);
            } catch (Exception e) {
                return provideAndStoreFormattedExcpetion(e);
            } finally {
                sftp.disconnect(); 
                
                //SEASUPPIP-72: Reliably closing the jdbc connection in a finally block.
                jdbc.closeJdbcConnection();
                }
    }
    
    
    //SEAPA00-23: Implemented a healthcheck endpoint, which runs a 'select from dual' against the database and returns the logs and a status code.
    //Should be called as liveness probe from kubernetes: https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/#define-a-liveness-http-request
    @GET
    @Produces("plain/text")
    @Path("/jndi_healthcheck")
    public String jndiHealthcheck(@HeaderParam("p_datasource_alias") String dataSourceAlias) throws AccessDeniedException {
            String HEALTHCHECK_QUERY = "select 'ok' as HEALTHCHECK_RESPONSE from dual";
            String EXPECTED_HEALTHCHECK_RESPONSE = "ok";
            String healthcheckResponse;
            String response = "";
            JdbcCommunicator jdbc = new JdbcCommunicator();
            
            Logger.isGenerateRuntimeSnippet = true;
            
            try {
                Logger.populateGlobalOperationIdentifier("JNDI_HEALTHCHECK");
                Logger.info("Executing JNDI_HEALTHCHECK with" + 
                             " p_datasource_alias:" + dataSourceAlias);
                
                //Eestablishing the JDBC connection inside the communicator
                jdbc.establishJdbcConnection(dataSourceAlias, testMode);
                
                healthcheckResponse = jdbc.querySingleValueViaSql(HEALTHCHECK_QUERY);
                
                Logger.info("Received healthcheckResponse from database: " + healthcheckResponse);
                
                if (healthcheckResponse.equals(EXPECTED_HEALTHCHECK_RESPONSE)){
                    Logger.info("healthcheckResponse matches the expected response. STATUS OK.");
                    response = provideAndStoreResponse(
                                SftpResponse.getSftpResponse(Logger.runtimeSnippet, "")
                                );
                    return response;
                } else{
                    throw new AccessDeniedException("Sample select against the DB returned an unexpected value:" + healthcheckResponse);
                    //return provideAndStoreFormattedExcpetion(new Exception("Sample select against the DB returned an unexpected value:" + healthcheckResponse));
                }
                
            }  catch (Exception e) {
                throw new AccessDeniedException("The connection using the given p_datasource_alias was not successful: " + e);
            }
            finally {
                Logger.isGenerateRuntimeSnippet = false;
                Logger.runtimeSnippet = "";
                
                //SEASUPPIP-72: Reliably closing the jdbc connection in a finally block.
                jdbc.closeJdbcConnection();
                }
    }
    
    @Provider
    public class AccessDeniedHandler
            implements javax.ws.rs.ext.ExceptionMapper<AccessDeniedException> {
        public javax.ws.rs.core.Response toResponse(AccessDeniedException exn) {
            return Response.status(500).type("text/plain")
                    .entity(exn).build();
        }
    }
    
    private String provideAndStoreResponse(String response){
        //log via log4j
        Logger.info("Operation successfully executed.");
        //generate REST response
        return response;
    }
    
    private String provideAndStoreFormattedExcpetion(Exception e){
        String formattedException = Logger.formatException(e);
        //log via log4j
        Logger.error("Caught an exception, while processing a REST-Request: " + "\r\n" + formattedException, e);
        //generate REST response
        return SftpResponse.getSftpResponse("", formattedException);
    }
}
