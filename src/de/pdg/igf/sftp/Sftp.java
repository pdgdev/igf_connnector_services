package de.pdg.igf.sftp;


import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Proxy;
import com.jcraft.jsch.ProxyHTTP;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

import de.pdg.igf.SftpService;
import de.pdg.igf.b64.Base64;

import de.pdg.igf.jdbc.JdbcCommunicator;
import de.pdg.igf.log.Logger;
import de.pdg.igf.sftp.dao.SftpResponse;
import de.pdg.igf.util.Regex;
import de.pdg.igf.util.VectorHandler;
import de.pdg.igf.util.ZipBundler;
import de.pdg.igf.util.ZipHandler;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import java.sql.SQLException;


public class Sftp {
    private String username;
    private String host;
    private String password;
    private String keyFilePath;
    private String keyFilePhrase;
    int PORT = 22;
    Channel channel;
    ChannelSftp sftpChannel;
    Session session;
    

    public Sftp(String username, String password, String host, String keyFilePath, String keyPhrase) {    
        this.username = username;
        this.host = host;
        this.password = password;
        this.keyFilePath = keyFilePath;
        this.keyFilePhrase = keyPhrase;
    }
    

    public void connect() throws JSchException {
        
        Logger.info("Sftp.connect..." + "\r\n" +
                            "  -> username: " + username + "\r\n" +
                            "  -> host: " + host +
                            "  -> keyFilePath: " + keyFilePath);
        
        JSch jsch = new JSch();
        java.util.Properties config = new java.util.Properties();
        
        if(keyFilePath != null){
            //Might come in handy later: System.getProperty("java.scratch.dir");
            if (keyFilePhrase != null){
                jsch.addIdentity(keyFilePath, keyFilePhrase);
                Logger.info("Sftp.connect: KeyFile (with Passphrase) added: " + keyFilePath);
            }else{
                jsch.addIdentity(keyFilePath);
                Logger.info("Sftp.connect: KeyFile added: " + keyFilePath);
            }
            config.put("PreferredAuthentications", "publickey");
        }

        session = jsch.getSession(username, host, PORT);
        
        if(password != null){
            session.setPassword(password);
            Logger.info("Sftp.connect: Using a password.");
        }
        
        config.put("StrictHostKeyChecking", "no");
        session.setConfig(config);
        
        //!!!!!! IMPORTANT, FOR USE ON JCS-SX !!!!!!!!
        //REVIEW: Doc ID 2197534.1
        //------------>
        if(!SftpService.testMode){
            String proxy_host = System.getProperty("http.proxyHost");
            String proxy_port = System.getProperty("http.proxyPort");

            Logger.info("Sftp.connect: proxy_host: " + proxy_host);
            Logger.info("Sftp.connect: proxy_port: " + proxy_port);
            
            // SEA-430: s.lindner - set proxy only if values are not null
            if (proxy_host != null && proxy_port != null){
                Proxy p = new ProxyHTTP(proxy_host, Integer.parseInt(proxy_port));
                session.setProxy(p);
            }
        }
        //<------------
        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        
        session.connect();

        channel = session.openChannel("sftp");
        channel.connect();
        sftpChannel = (ChannelSftp)channel;
        
        Logger.info("Sftp.connect succeeded.");   
    }
    
    public void connectNoProxy() throws JSchException {
        
        Logger.info("Sftp.connectNoProxy..." + "\r\n" +
                            "  -> username: " + username + "\r\n" +
                            "  -> port: " + PORT + "\r\n" +
                            "  -> host: " + host);
        
        JSch jsch = new JSch();

        Session session = jsch.getSession(username, host, PORT);
        
        
        session.setPassword(password);
        java.util.Properties config = new java.util.Properties();
        config.put("StrictHostKeyChecking", "no");
        session.setConfig(config);

        session.connect();
        
        channel = session.openChannel("sftp");
        channel.connect();
        sftpChannel = (ChannelSftp)channel;
    
        Logger.info("Sftp.connectNoProxy succeeded.");   
    }
    
    public void disconnect() {
        Logger.info("Sftp.disconnect...sftpChannel, channel and session....");
        
        //Making sure to close the SFTP ressources by any means.
        try { sftpChannel.disconnect();  } catch (Exception e) { /* Ignored */ }
        try { sftpChannel.exit();  } catch (Exception e) { /* Ignored */ }
        try { channel.disconnect();  } catch (Exception e) { /* Ignored */ }
        try { session.disconnect();  } catch (Exception e) { /* Ignored */ }
        
        if(sftpChannel != null && channel != null && session != null){
            Logger.info("SFTP Connection statuses:" + "\r\n" +
                        "sftpChannel is connected = " + sftpChannel.isConnected() + "\r\n" + 
                        "sftpChannel is closed = " + sftpChannel.isClosed() + "\r\n" +
                        "channel is connected = " + channel.isConnected() + "\r\n" + 
                        "channel is closed = " + channel.isClosed() + "\r\n" +
                        "session is connected = " + session.isConnected()); 
        }
    }
    

    public String ls(String path, Boolean filesOnly) throws SftpException {
        
        Logger.info("Sftp.ls..." + "\r\n" +
                            "  -> path: " + path);
        
        String retString = "";
        java.util.Vector vv = sftpChannel.ls(path);
        
        retString = VectorHandler.toString(vv, filesOnly);

        Logger.info("Sftp.ls succeeded.");   
        return retString;
    }
    
    public String[] lsArray(String path, Boolean filesOnly) throws SftpException {
        
        Logger.info("Sftp.lsArray..." + "\r\n" +
                            "  -> path: " + path);
        
        java.util.Vector vv = sftpChannel.ls(path);
        
        String[] retArray = VectorHandler.toStringArray(vv, filesOnly);
         
        Logger.info("Sftp.lsArray succeeded.");
        return retArray;
    }

    public String lsla(String path, Boolean filesOnly) throws SftpException {
        
        Logger.info("Sftp.lsla..." + "\r\n" +
                            "  -> path: " + path);
        
        String retString = "";
        java.util.Vector vv = sftpChannel.ls(path);
        
        retString = VectorHandler.toString(vv, filesOnly);

        Logger.info("Sftp.lsla succeeded.");   
        return retString;
    }
    
    public String mkdir(String path) throws SftpException {
        
        Logger.info("Sftp.mkdir..." + "\r\n" +
                            "  -> path: " + path);
        
        String successMessage = "Directory <" + path + "> successfully created.";
        sftpChannel.mkdir(path);
        sftpChannel.cd(path);
        
        Logger.info("Sftp.mkdir succeeded: " + successMessage); 
        return successMessage;
    }
    
    public String rmdir(String path) throws SftpException {
        
        Logger.info("Sftp.rmdir..." + "\r\n" +
                            "  -> path: " + path);
        
        String successMessage = "Directory <" + path + "> successfully deleted.";
        sftpChannel.rmdir(path);
        
        Logger.info("Sftp.rmdir succeeded: " + successMessage);   
        return successMessage;
    }
    
    public String rm(String path) throws SftpException {
        
        Logger.info("Sftp.rm..." + "\r\n" +
                            "  -> path: " + path);
        
        String successMessage = "File <" + path + "> successfully deleted.";
        sftpChannel.rm(path);
    
        Logger.info("Sftp.rm succeeded: " + successMessage);   
        return successMessage;
    }
    
    public String mv(String old_path, String new_path) throws SftpException {
        
        Logger.info("Sftp.mv..." + "\r\n" +
                            "  -> old_path: " + old_path + "\r\n" +
                            "  -> new_path: " + new_path);
        
        String successMessage = "Renamed file <" + old_path + "> successfully to <" + new_path + ">.";
        sftpChannel.rename(old_path, new_path);
        
        Logger.info("Sftp.mv succeeded: " + successMessage);   
        return successMessage;
    }
    
    public String get_base64(String path) throws SftpException, IllegalArgumentException {
        
        Logger.info("Sftp.get_base64..." + "\r\n" +
                            "  -> path: " + path);
        
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        String base64encodedFile = "";
        sftpChannel.get(path, outStream);
        if(outStream.size()<=0){
            throw new SftpException(-1, "The requested file has 0 bytes and is ignored. (Path: " + path +")");
        }else{
            base64encodedFile = Base64.encodeLines(outStream.toByteArray());
        }
        
        Logger.info("Sftp.get_base64 succeeded.");   
        return base64encodedFile;
    }
    
    public ByteArrayOutputStream get(String path) throws SftpException, IllegalArgumentException {
        
        Logger.info("Sftp.get..." + "\r\n" +
                            "  -> path: " + path);
        
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();

        sftpChannel.get(path, outStream);
        if(outStream.size()<=0){
            throw new SftpException(-1, "The requested file has 0 bytes and is ignored. (Path: " + path +")");
        }else{
            Logger.info("Sftp.get succeeded.");   
            return outStream;
        }    
    }
    
    public String put(String path, String b64EncodedFile) throws SftpException, IllegalArgumentException {
        
        Logger.info("Sftp.put..." + "\r\n" +
                            "  -> path: " + path);
        
        String successMessage = "Successfully put the file <" + path + ">.";
        InputStream is = new ByteArrayInputStream(Base64.decode(b64EncodedFile));
        sftpChannel.put(is, path);
        
        Logger.info("Sftp.put succeeded: " + successMessage);   
        return successMessage;
    }
    
    public String put(String path, byte[] fileBytes) throws SftpException, IllegalArgumentException {
        
        Logger.info("Sftp.put..." + "\r\n" +
                            "  -> path: " + path);
        
        String successMessage = "Successfully put the file <" + path + ">.";
        InputStream is = new ByteArrayInputStream(fileBytes);
        sftpChannel.put(is, path);
        
        Logger.info("Sftp.put succeeded: " + successMessage);   
        return successMessage;
    }
    
    public String toString(){  
        return username + ":" + password + ":" + host + ":" + PORT + sftpChannel.isConnected();
    }
    
    public String storeSingleFilesNoncompressed(String[] fileListing, String fileNamePattern, String path, JdbcCommunicator jdbc, Sftp sftp) 
    throws SQLException,IOException,SftpException,ClassNotFoundException {
        
        Logger.info("Sftp.storeSingleFilesNoncompressed..." + "\r\n" +
                            "  -> fileNamePattern: " + fileNamePattern + "\r\n" +
                            "  -> path: " + path);
        
        String currentTempFileToken = "";
        String compressedFlag = "N";
        int fileCounter = 0;
        
        String testString = "";
        for(String fileName : fileListing){
            testString = testString + "--> " + fileName + "\r\n";
        }
        Logger.info(testString);
        
        for(String fileName : fileListing){
            if(fileName != null){
                if(Regex.regexCheck(fileName, fileNamePattern)){
                    fileCounter++;
                
                    //Create File-MetaData in IGF_TEMP_FILES
                    currentTempFileToken = jdbc.insertFileMetadata(currentTempFileToken, fileName, compressedFlag);
                
                    //Get file as ByteArrayOutputStream and upload to BLOB_DATA
                    ByteArrayOutputStream baos = sftp.get(path+fileName);
                    jdbc.insertTempFilesBlob(currentTempFileToken, fileName, baos.toByteArray());
                }
            }
        }
        if (fileCounter > 0){
            Logger.info("Sftp.storeSingleFilesNoncompressed succeeded.");   
            return SftpResponse.getSftpResponse(currentTempFileToken, "");
        }else{
            Logger.info("Sftp.storeSingleFilesNoncompressed: " + "No file is matching the given regex: '" + fileNamePattern + "'.");   
            return SftpResponse.getSftpResponse("No file is matching the given regex: '" + fileNamePattern + "'." , "");
        }
    }
    
    public String storeSingleFilesCompressed(String[] fileListing, String fileNamePattern, String path, JdbcCommunicator jdbc, Sftp sftp) 
    throws SQLException,IOException,SftpException,ClassNotFoundException {
        
        Logger.info("Sftp.storeSingleFilesCompressed..." + "\r\n" +
                            "  -> fileNamePattern: " + fileNamePattern + "\r\n" +
                            "  -> path: " + path);
        
        String currentTempFileToken = "";        
        String compressedFlag = "Y";
        int fileCounter = 0;
        byte[] fileBytes;
        
        for(String fileName : fileListing){
            if(Regex.regexCheck(fileName, fileNamePattern)){
                fileCounter++;
                
                //Create File-MetaData in IGF_TEMP_FILES
                currentTempFileToken = jdbc.insertFileMetadata(currentTempFileToken, fileName + ".zip", compressedFlag);
                
                //ByteArrayOutputStream goes in
                ByteArrayOutputStream baos = sftp.get(path+fileName);
                fileBytes = ZipHandler.zipBytes(fileName, baos.toByteArray());
                
                //byte[] goes in     
                jdbc.insertTempFilesBlob(currentTempFileToken, fileName + ".zip", fileBytes);
            }
        }
        if (fileCounter > 0){
            Logger.info("Sftp.storeSingleFilesCompressed succeeded.");   
            return SftpResponse.getSftpResponse(currentTempFileToken, "");
        }else{
            Logger.info("Sftp.storeSingleFilesCompressed: " + "No file is matching the given regex: '" + fileNamePattern + "'.");   
            return SftpResponse.getSftpResponse("No file is matching the given regex: '" + fileNamePattern + "'." , "");
        }
    }
    
    public String storeFilesAsBundle(String[] fileListing, String fileNamePattern, String path, JdbcCommunicator jdbc, Sftp sftp) 
    throws SQLException,IOException,SftpException,ClassNotFoundException {
        
        Logger.info("Sftp.storeFilesAsBundle..." + "\r\n" +
                            "  -> fileNamePattern: " + fileNamePattern + "\r\n" +
                            "  -> path: " + path);
        
        String currentTempFileToken = "";        
        String compressedFlag = "Y";
        int fileCounter = 0;
        ZipBundler bundler = new ZipBundler();
        
        //Create File-MetaData in IGF_TEMP_FILES
        currentTempFileToken = jdbc.insertFileMetadata(currentTempFileToken, "bundle.zip", compressedFlag);
        
        for(String fileName : fileListing){
            if(Regex.regexCheck(fileName, fileNamePattern)){
                fileCounter++;
                
                //ByteArrayOutputStream goes in
                ByteArrayOutputStream baos = sftp.get(path+fileName);
                bundler.addEntry(fileName, baos.toByteArray());
            }
        }
        if (fileCounter > 0){
            jdbc.insertTempFilesBlob(currentTempFileToken, "bundle.zip", bundler.getBundle());
            
            Logger.info("Sftp.storeFilesAsBundle succeeded.");   
            return SftpResponse.getSftpResponse(currentTempFileToken, "");
        }else{
            
            Logger.info("Sftp.storeFilesAsBundle: " + "No file is matching the given regex: '" + fileNamePattern + "'.");   
            return SftpResponse.getSftpResponse("No file is matching the given regex: '" + fileNamePattern + "'." , "");
        }
    }
}
