package de.pdg.igf.sftp.dao;

import javax.ws.rs.Produces;

import javax.xml.bind.annotation.XmlRootElement;


@Produces("application/json")
@XmlRootElement
public class SftpFile {
    private String fileBase64;
    
    public SftpFile() {} 

    public void setFileBase64(String fileBase64) {
        this.fileBase64 = fileBase64;
    }

    public String getFileBase64() {
        return fileBase64;
    }
}