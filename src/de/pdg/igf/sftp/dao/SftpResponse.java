package de.pdg.igf.sftp.dao;

public class SftpResponse {
    private static String jsonBasic = "{\"SftpResponse\":{\"Response\":\"$1\",\"Error-Message\":\"$2\"}}";
    
    public static String getSftpResponse(String response, String message){
        String jsonResponse = "";
        jsonResponse = jsonBasic.replace("$1", response);
        jsonResponse = jsonResponse.replace("$2", message);
        return jsonResponse;
    }
}

