package de.pdg.igf.jdbc;

import de.pdg.igf.log.Logger;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;

import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;

import oracle.jdbc.OracleResultSet;

import oracle.sql.BLOB;


public class JdbcCommunicator {
    Connection connection = null;

    public JdbcCommunicator() { }
    
    public void establishJdbcConnection(String dataSourceAlias, Boolean testMode) throws SQLException, NamingException {
        
        if(testMode){
        System.out.println("We are in TEST-mode!");
            connection = JdbcConnectionManager.getTestingConnection();
        }else {
            connection = JdbcConnectionManager.getJndiConnection(dataSourceAlias);
        }
    }
    
    public void closeJdbcConnection(){
        String isClosed = "";
        try { connection.close(); isClosed = connection.isClosed()?"true":"false";} catch (Exception e) { /* Ignored */ }
        if(connection != null){
            Logger.info("JDBC Connection was closed." + "\r\n" +
                                "  -> Connection is closed? ---> " + isClosed);
        }
    }

    public String querySingleValueViaSql(String query) {
        Statement stmt = null;
        ResultSet rs = null;
        
        String queryResponse = "";
        
        Logger.info("JdbcCommunicator.querySingleValueViaSql..." + "\r\n" +
                            "  -> query: " + query);
           
        try {
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);
            
            while (rs.next()) {
                    queryResponse = rs.getString(1);
            }
            
            return queryResponse;
            
        } catch (SQLException e) {
            e.printStackTrace();
        } finally{
            //SEASUPPIP-72: Reliably closing the result set and statement in a finally block.
            try { rs.close(); } catch (Exception e) { /* Ignored */ }
            try { stmt.close(); } catch (Exception e) { /* Ignored */ }
        }
        return null;
    }

    public String insertFileMetadata(String token, String fileName, String compressedFlag) throws SQLException {
        
        Logger.info("JdbcCommunicator.insertFileMetadata..." + "\r\n" +
                            "  -> token: " + token + "\r\n" +
                            "  -> fileName: " + fileName + "\r\n" +
                            "  -> compressedFlag: " + compressedFlag);
        
        String insertTemplatePlsql =
            "declare " + "l_vc2 varchar2(100) := '$1'; " + 
            "begin " +
            "igf_temp_files_tpi.jdbc_insert_template(p_file_name       => '" + "$2" + "'" + 
            ",p_compressed_flag  => '"+ compressedFlag +"'" +
            ",px_token            => l_vc2); " + 
            "? := l_vc2; " + 
            "end;";
        
        insertTemplatePlsql = insertTemplatePlsql.replace("$1",token.equals("")?"null":token);
        insertTemplatePlsql = insertTemplatePlsql.replace("$2",fileName);
        CallableStatement cs = connection.prepareCall(insertTemplatePlsql);
        cs.registerOutParameter(1, Types.VARCHAR);
            
        try{
            cs.execute();
            token = (String)cs.getObject(1);
            Logger.info("JdbcCommunicator.insertFileMetadata succeeded.");
            return token;
        } catch (SQLException e){
            throw e;
        } finally{
            //SEASUPPIP-72: Reliably closing the result set and statement in a finally block.
            try { cs.close(); } catch (Exception e) { /* Ignored */ }
        }
    }
    
    public void insertTempFilesBlob(String token, String fileName, byte[] fileBytes) 
    throws SQLException,IOException {
        
        Logger.info("JdbcCommunicator.insertTempFilesBlob..." + "\r\n" +
                            "  -> token: " + token + "\r\n" +
                            "  -> fileName: " + fileName);
        
        Statement stmt = null;
        String sql = "SELECT blob_data FROM igf_temp_files WHERE file_name = '" + fileName +
              "' and token = '" + token + "' FOR UPDATE";
        
        ResultSet rs = null;
        BLOB blob = null;
        ByteArrayInputStream is;
        int chunkSize;
        byte[] byteBuffer;
        long position = 1;
        int offset = 0;
        int bytesRead = 0;
        int bytesWritten = 0;
        
        try{
            stmt = connection.createStatement();
    
            rs = stmt.executeQuery(sql);
            rs.next();
        
            //blob = ((OracleResultSet)rset).getBLOB("blob_data");
            blob = (oracle.sql.BLOB) rs.getBlob("blob_data");

            chunkSize = blob.getChunkSize();
            byteBuffer = new byte[chunkSize];

            is = new ByteArrayInputStream(fileBytes);
        
            while ((bytesRead = is.read(byteBuffer)) != -1) {
                bytesWritten = blob.setBytes(position, byteBuffer, offset, bytesRead);
                position += bytesRead;
            }
        
            is.close();

            connection.commit();
            Logger.info("JdbcCommunicator.insertTempFilesBlob succeeded.");
        } catch (SQLException e){
            throw e;
        } catch (IOException e){
            throw e;  
        } finally{
            //SEASUPPIP-72: Reliably closing the result set and statement in a finally block.
            try { rs.close(); } catch (Exception e) { /* Ignored */ }
            try { stmt.close(); } catch (Exception e) { /* Ignored */ }
        }
    }
    
    public ArrayList<byte[]> getFilesFromBlob(String token) throws SQLException {
        
        Logger.info("JdbcCommunicator.getFilesFromBlob..." + "\r\n" +
                            "  -> token: " + token);
        
        Statement stmt = null;
        ResultSet rs = null;
        
        String sql = "SELECT file_name, blob_data FROM igf_temp_files where token = '" + token + "'";
        
        try{
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);
            Logger.info("JdbcCommunicator.getFilesFromBlob succeeded.");
            
            ArrayList<byte[]> byteArrayList = new ArrayList<byte[]>();
            
            while(rs.next()){
                Blob blob = rs.getBlob("blob_data");
                int blobLength = (int) blob.length();  
                byte[] blobAsBytes = blob.getBytes(1, blobLength);
                byteArrayList.add(blobAsBytes);
                blob = null;
            }
            
            
            return byteArrayList;
        } catch (SQLException e){
            throw e;
        } finally{
            //SEASUPPIP-72: Reliably closing the result set and statement in a finally block.
            try { rs.close(); } catch (Exception e) { /* Ignored */ }
            try { stmt.close(); } catch (Exception e) { /* Ignored */ } 
        }
    }
    
    public ResultSet getFilesFromClob(String token) throws SQLException {
        
        Logger.info("JdbcCommunicator.getFilesFromClob..." + "\r\n" +
                            "  -> token: " + token);
        
        Statement stmt = null;
        ResultSet rs = null;
        String sql = "SELECT file_name, clob_data FROM igf_temp_files where token = '" + token + "'";
        
        try{
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);
            Logger.info("JdbcCommunicator.getFilesFromClob succeeded.");
            return rs;
        } catch (SQLException e){
            throw e;
        } finally{
            //SEASUPPIP-72: Reliably closing the result set and statement in a finally block.
            try { rs.close(); } catch (Exception e) { /* Ignored */ }
            try { stmt.close(); } catch (Exception e) { /* Ignored */ }
        }
    }
}
