package de.pdg.igf.jdbc;

import de.pdg.igf.log.Logger;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import oracle.jdbc.pool.OracleDataSource;


public class JdbcConnectionManager {

    public static Connection getNewConnection(String connectionString, String username, String password) throws SQLException {
        
        Logger.info("JdbcConnectionManager.getNewConnection..." + "\r\n" +
                            "  -> connectionString: " + connectionString + "\r\n" +
                            "  -> username: " + username + "\r\n" +
                            "  -> password: " + "XXXXXX");
        
        Connection connection;

        final OracleDataSource ods = new OracleDataSource();
        ods.setURL(connectionString);
        ods.setUser(username);
        ods.setPassword(password);

        connection = ods.getConnection();
        
        Logger.info("JdbcConnectionManager.getNewConnection succeeded.");   
        return connection;
    }
    
    public static Connection getTestingConnection() throws SQLException {
        
        Logger.info("JdbcConnectionManager.getTestingConnection...");
        
        Connection connection;
        String jdbc_connection_string = "jdbc:oracle:thin:@primus10.primus-solutions.de:1523:PS02";
        String jdbc_username = "CLX";
        String jdbc_password = "PDGCloudXchange";

        final OracleDataSource ods = new OracleDataSource();
        ods.setURL(jdbc_connection_string);
        ods.setUser(jdbc_username);
        ods.setPassword(jdbc_password);

        connection = ods.getConnection();
        
        Logger.info("JdbcConnectionManager.getTestingConnection succeeded.");   
        return connection;
    }
    
    public static Connection getJndiConnection(String dataSourceAlias) throws NamingException, SQLException {
        Logger.info("JdbcConnectionManager.getJndiConnection..." + "\r\n" + "  -> dataSourceAlias: " + dataSourceAlias);
        
        Connection connection;
        Context ctx = new InitialContext();
        javax.sql.DataSource ds = (javax.sql.DataSource) ctx.lookup("java:comp/env/" + dataSourceAlias);
        
        connection = ds.getConnection();
        // disable auto commit cause of exception in JdbcCommunicator.insertTempFilesBlob ("commit not allowed, when autoCommit is active")
        connection.setAutoCommit(false);

        Logger.info("JdbcConnectionManager.getJndiConnection succeeded.");   
        return connection;
    }
}
