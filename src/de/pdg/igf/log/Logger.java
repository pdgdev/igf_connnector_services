package de.pdg.igf.log;

import java.text.SimpleDateFormat;

import java.util.Date;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.SimpleLayout;

/**
 * Available message-types:
 *  logger.debug("My Debug-MSG");   - Maps to "TRACE" in JCS-SX.
 *  logger.info("My Info-MSG");     - Maps to "NOTIFICATION" in JCS-SX.
 *  logger.warn("My Warn-MSG");     - Maps to "WARNING" in JCS-SX.
 *  logger.error("My Error-MSG");   - Maps to "ERROR" in JCS-SX.
 *  logger.fatal("My Fatal-MSG");   - Maps to "ERROR" in JCS-SX.
 */
public class Logger {
    private static String globalOperationIdentifier;
    private static org.apache.log4j.Logger l4jLogger;
    
    //SEAPA00-23: Adding functinality to preserve a runtime log 
    public static Boolean isGenerateRuntimeSnippet = false;
    public static String runtimeSnippet = new String();
    
    private static void guaranteeLoggerPresence(){
        if (l4jLogger == null){
            initLog4JLogger();
        }
    }
    
    private static void initLog4JLogger(){
        org.apache.log4j.Logger logger = org.apache.log4j.Logger.getRootLogger();
        SimpleLayout layout = new SimpleLayout();
        ConsoleAppender consoleAppender = new ConsoleAppender(layout);
        logger.addAppender(consoleAppender);
             
        l4jLogger = logger;
    }
    
    private static void preserveRuntimeSnippet(String logEntry){
        runtimeSnippet = runtimeSnippet + "\r\n" + logEntry;
    }
    
    public static void debug(String message){
        guaranteeLoggerPresence();
        
        String logEntry = prependOperationIdent(message);
        if(isGenerateRuntimeSnippet){preserveRuntimeSnippet(logEntry);}
        
        l4jLogger.debug(logEntry);
    }
    
    public static void debug(String message, Throwable throwable){
        guaranteeLoggerPresence();
        
        String logEntry = prependOperationIdent(message);
        if(isGenerateRuntimeSnippet){preserveRuntimeSnippet(logEntry);}
        l4jLogger.debug(logEntry, throwable);
    }
    
    public static void info(String message){
        guaranteeLoggerPresence();
        
        String logEntry = prependOperationIdent(message);
        if(isGenerateRuntimeSnippet){preserveRuntimeSnippet(logEntry);}
        l4jLogger.info(logEntry);
    }
    
    public static void info(String message, Throwable throwable){
        guaranteeLoggerPresence();
        
        String logEntry = prependOperationIdent(message);
        if(isGenerateRuntimeSnippet){preserveRuntimeSnippet(logEntry);}
        l4jLogger.info(logEntry, throwable);
    }
    
    public static void warn(String message){
        guaranteeLoggerPresence();
        
        String logEntry = prependOperationIdent(message);
        if(isGenerateRuntimeSnippet){preserveRuntimeSnippet(logEntry);}
        l4jLogger.warn(logEntry); 
    }
    
    public static void warn(String message, Throwable throwable){
        guaranteeLoggerPresence();
        
        String logEntry = prependOperationIdent(message);
        if(isGenerateRuntimeSnippet){preserveRuntimeSnippet(logEntry);}
        l4jLogger.warn(logEntry, throwable); 
    }
    
    public static void error(String message){
        guaranteeLoggerPresence();
        
        String logEntry = prependOperationIdent(message);
        if(isGenerateRuntimeSnippet){preserveRuntimeSnippet(logEntry);}
        l4jLogger.error(logEntry);
    }
    
    public static void error(String message, Throwable throwable){
        guaranteeLoggerPresence();
        
        String logEntry = prependOperationIdent(message);
        if(isGenerateRuntimeSnippet){preserveRuntimeSnippet(logEntry);}
        l4jLogger.error(logEntry, throwable);
    }
    
    public void fatal(String message){
        guaranteeLoggerPresence();
        
        String logEntry = prependOperationIdent(message);
        if(isGenerateRuntimeSnippet){preserveRuntimeSnippet(logEntry);}
        l4jLogger.fatal(logEntry);
    }
    
    public void fatal(String message, Throwable throwable){
        guaranteeLoggerPresence();
        
        String logEntry = prependOperationIdent(message);
        if(isGenerateRuntimeSnippet){preserveRuntimeSnippet(logEntry);}
        l4jLogger.fatal(logEntry, throwable);
    }
    
    public static void populateGlobalOperationIdentifier(String operationName){
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyykkmmssSS");
        String datePrefix = sdf.format(date);
        globalOperationIdentifier = datePrefix + "_" + operationName;
    }
    
    private static String prependOperationIdent(String message){
        return globalOperationIdentifier + ": " + message;
    }
    
    public static String formatException(Exception e) {
        String errorstring = e + " | " + "\r\n";
        StackTraceElement error[] = e.getStackTrace();
        for (StackTraceElement element : error) {
            errorstring =
                    errorstring + element.getClassName() + " | " + element.getLineNumber() +
                    "\r\n";
        }

        return errorstring;
    }
}
