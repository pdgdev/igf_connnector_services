package de.pdg.igf;

import de.pdg.igf.log.Logger;
import de.pdg.igf.sftp.dao.SftpResponse;
import de.pdg.igf.util.WaitHandler;

import de.pdg.igf.util.dao.UtilResponse;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;


@Path("util")
public class UtilService {

    @GET
    @Produces("plain/text")
    @Path("/wait")
    public String wait(@HeaderParam("p_seconds")String seconds) {
        long waitingSeconds = Long.parseLong(seconds);
        String waitResponse;
        try {
            Logger.populateGlobalOperationIdentifier("WAIT");
            Logger.info("Executing WAIT with" + 
                         " p_seconds:" + seconds);
            
            waitResponse = WaitHandler.waitForSeconds(waitingSeconds);
            return provideAndStoreResponse(UtilResponse.getUtilResponse(waitResponse,""));
        } catch (InterruptedException e) {
            return provideAndStoreFormattedExcpetion(e);
        }
    }
    
    private String provideAndStoreResponse(String response){
        //log via log4j
        Logger.info("Operation successfully executed.");
        //generate REST response
        return response;
    }
    
    private String provideAndStoreFormattedExcpetion(Exception e){
        String formattedException = Logger.formatException(e);
        //log via log4j
        Logger.error("Caught an exception, while processing a REST-Request: " + "\r\n" + formattedException, e);
        //generate REST response
        return UtilResponse.getUtilResponse("", formattedException);
    }
}
