#!/bin/sh

JAVA_OPTS="$JAVA_OPTS -Ddb.host=$DB_HOST "
JAVA_OPTS="$JAVA_OPTS -Ddb.port=$DB_PORT"
JAVA_OPTS="$JAVA_OPTS -Ddb.servicename=$DB_SERVICENAME"
JAVA_OPTS="$JAVA_OPTS -Ddb.username=$DB_USERNAME"
JAVA_OPTS="$JAVA_OPTS -Ddb.password=$DB_PASSWORD"
JAVA_OPTS="$JAVA_OPTS -Digf.username=$IGF_USERNAME"
JAVA_OPTS="$JAVA_OPTS -Digf.password=$IGF_PASSWORD"
export JAVA_OPTS