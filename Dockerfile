# ------------------------------------------------------------------------------
# Dockerfile to build IGF SFTP Connector Service images
# Based on the following:
#   - OpenJDK 8
#   - Apache Tomcat 9.0.22
#   - PDG IGF SFTP Connector
#
# Example build and run:
#
# docker build -t pdg/igf_sftp_connector_service:latest .
#
# docker run -dit --name igf_sftp_connector_service_con -p 8080:8080 pdg/igf_sftp_connector_service:latest
#
# docker logs --follow igf_sftp_connector_service_con
# docker exec -it igf_sftp_connector_service_con bash
#
# docker start igf_sftp_connector_service_con
# docker stop --time=30 igf_sftp_connector_service_con
#
# docker rm -vf igf_sftp_connector_service_con
#
# ------------------------------------------------------------------------------

# Set the base image to Tomcat 9 with OpenJDK 8 (slim version)
FROM tomcat:9-jdk8-openjdk-slim

# File Author / Maintainer
LABEL maintainer="s.lindner@primus-delphi-group.com"

# change tomcat configuration
ADD ./docker-files/setenv.sh /usr/local/tomcat/bin/
ADD ./docker-files/server.xml /usr/local/tomcat/conf/
ADD ./docker-files/context.xml /usr/local/tomcat/conf/
ADD ./docker-files/tomcat-users.xml /usr/local/tomcat/conf/
ADD ./public_html/WEB-INF/lib/ojdbc8.jar /usr/local/tomcat/lib

# deploy application to the image
ADD ./deploy/IGF_Connectorv1.7.war /usr/local/tomcat/webapps/IGF_Connector_Services.war

VOLUME ["/u01/app/pdg/product"]

# expose tomcat port
EXPOSE 8080

# start tomcat.
CMD ["catalina.sh", "run"]

# End